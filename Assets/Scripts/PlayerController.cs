﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public float speed = 5;
	public float jumpForce = 150;
	public float doubleJumpForce = 100;

	Transform helpTextCanvas;

	float moveX;
	float moveXSmoothing = 0.04f;
	public float moveXAirSmoothing = 0.4f;

	bool grounded;
	bool frozen;
	bool haveDoubleJumped = false;
	bool stuckFrozen;

	Rigidbody2D rb2d;
	Color mainColor;
	MagnetEffect magnet;

	void Awake () {
		rb2d = GetComponent<Rigidbody2D>();
		magnet = GetComponentInChildren<MagnetEffect>();
		magnet.attachedRigidbody = rb2d;
		grounded = transform.parent.GetComponentInChildren<GroundCheck>().grounded;
		frozen = GetComponentInChildren<FreezeCheck>().frozen;
		stuckFrozen = GetComponentInChildren<FreezeCheck>().stuckFrozen;
		mainColor = gameObject.GetComponent<SpriteRenderer>().color;
		helpTextCanvas = GameObject.Find("HelpTextCanvas").transform;
	}

	// buttons
	bool restartButton () {
		return Input.GetKeyDown(KeyCode.R);
	}
	bool jumpButton () {
		return Input.GetKeyDown(KeyCode.W);
	}
	bool magnetButtonPositive () {
		return Input.GetMouseButton(1);
	}
	bool magnetButtonNegative () {
		return Input.GetMouseButton(0);
	}

	// Update is called once per frame
	void Update () {
		float forwardVelocity = 0;
		grounded = transform.parent.GetComponentInChildren<GroundCheck>().grounded;
		frozen = GetComponentInChildren<FreezeCheck>().frozen;
		stuckFrozen = GetComponentInChildren<FreezeCheck>().stuckFrozen;
		if (grounded)
			haveDoubleJumped = true;

		// debug aka colors...
		if (frozen)
			gameObject.GetComponent<SpriteRenderer>().color = Color.grey;
		else if (magnet.activated && magnet.positive)
			gameObject.GetComponent<SpriteRenderer>().color = Color.red;
		else if (magnet.activated && !magnet.positive)
			gameObject.GetComponent<SpriteRenderer>().color = Color.blue;
		else
			gameObject.GetComponent<SpriteRenderer>().color = mainColor;

		// HelpText
		if (stuckFrozen) {
			if (helpTextCanvas != null) {
				if (helpTextCanvas.Find("RestartPanel").gameObject.activeInHierarchy == false) {
					helpTextCanvas.Find("RestartPanel").gameObject.SetActive(true);
				}
			}
		} else {
			if (helpTextCanvas != null) {
				if (helpTextCanvas.Find("RestartPanel").gameObject.activeInHierarchy == true) {
					helpTextCanvas.Find("RestartPanel").gameObject.SetActive(false);
				}
			}
		}
			

		// Respawn player
		if (restartButton()) {
			Destroy(transform.parent.gameObject);
			FindObjectOfType<SpawnPlayer>().Spawn();
		}

		// Moves that are not allowed when frozen
		if (!frozen) {
			
			// Activate magnet
			if (magnetButtonPositive() || magnetButtonNegative())
				magnet.activated = true;
			else if (!magnetButtonPositive() && !magnetButtonNegative())
				magnet.activated = false;
			
			// Change magnet type
			if (magnetButtonPositive())
				magnet.positive = true;
			else if (magnetButtonNegative())
				magnet.positive = false;

			// Jump & Doublejump
			if (jumpButton()) {
				if (!grounded && haveDoubleJumped) {
					rb2d.velocity = new Vector2(rb2d.velocity.x, 0);
					haveDoubleJumped = false;
					rb2d.AddForce(new Vector2(0, doubleJumpForce), ForceMode2D.Impulse);
				} else if (grounded) {
					rb2d.velocity = new Vector2(rb2d.velocity.x, 0);
					rb2d.AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);
				}
			}
		}

		// Movement | Different moveX for different situations
		if (!frozen) {
			// normal movement
			moveX = Mathf.SmoothDamp(moveX, Input.GetAxisRaw("Horizontal"), ref forwardVelocity, moveXSmoothing);
		}
		else {
			// movement in freeze when on ground
			if (grounded)
				moveX = Mathf.SmoothDamp(moveX, 0, ref forwardVelocity, moveXSmoothing);
			// movement in freeze when in air
			else
				moveX = Mathf.SmoothDamp(moveX, 0, ref forwardVelocity, moveXAirSmoothing);
		}
		rb2d.velocity = new Vector2(moveX * speed, rb2d.velocity.y);

	}

	void LateUpdate()
	{
		Camera.main.GetComponent<MimicTransform>().UpdateTargetTransform(rb2d.transform);
	}

	// Update physical things here
	void FixedUpdate() {
		
	}
}

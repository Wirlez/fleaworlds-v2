﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour {

	public bool levelTeleporter = false;
	public string levelToTeleportTo;

	GameController game;

	void Awake()
	{
		game = GameObject.FindGameObjectWithTag("Game").GetComponent<GameController>();
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.layer == LayerMask.NameToLayer("Player")) {

			if (levelTeleporter)
				game.ChangeLevel(levelToTeleportTo);
		}
	}
}

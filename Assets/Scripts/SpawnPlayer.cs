﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPlayer : MonoBehaviour {

	public bool spawn = true;
	public GameObject playerPrefab;

	void Awake () {
		Spawn();
	}

	public void Spawn(){
		if (spawn) {
			print("Spawning player");
			Instantiate(playerPrefab, transform.position, Quaternion.identity);
		}
	}
}

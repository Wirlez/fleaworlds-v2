﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MimicTransform : MonoBehaviour {

	public Transform target;
	public float offsetX = 0;
	public float offsetY = 0;
	public float offsetZ = 0;

	void Update(){
		if (target) {
			transform.position = new Vector3(
				target.position.x + offsetX, 
				target.position.y + offsetY, 
				target.position.z + offsetZ);
		}
	}

	public void UpdateTargetTransform(Transform target_)
	{
		target = target_;
	}
}

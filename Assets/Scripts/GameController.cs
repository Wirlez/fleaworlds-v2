﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameController : MonoBehaviour {

	public bool paused = false;
	public Transform pauseMenuCanvas;
	float originalTimeScale;

	// Use this for initialization
	void Awake () {
		originalTimeScale = Time.timeScale;
	}
	
	// Update is called once per frame
	void Update () {
		// pause game
		if (Input.GetKeyDown(KeyCode.Escape)) {
			PauseGameToggle();
		}
	}

	public void ChangeLevel(string level)
	{
		SceneManager.LoadScene(level);
	}

	// todo: change this to save current scene and load pausemenu
	public void PauseGameToggle()
	{
		if (pauseMenuCanvas != null) {
			if (pauseMenuCanvas.gameObject.activeInHierarchy == false) {
				Time.timeScale = 0;
				pauseMenuCanvas.gameObject.SetActive(true);
			} else {
				pauseMenuCanvas.gameObject.SetActive(false);
				Time.timeScale = originalTimeScale;
			}
		}
	}

	public void QuitGame()
	{
		#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
		#else
			Application.Quit();
		#endif
	}
}

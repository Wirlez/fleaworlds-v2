﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreezeCheck : MonoBehaviour {

	public bool frozen = false;
	public float frozenSec = 3f;
	public bool stuckFrozen = false;

	float timeFrozen;
	float timeLeft;
	bool startCountDown = false;
	bool startCountUp = false;

	void Awake()
	{
		timeLeft = frozenSec;
	}

	void Update()
	{
		if (startCountUp) {
			timeFrozen += Time.deltaTime;
			if (timeFrozen > frozenSec+1)
				stuckFrozen = true;
		}
		if (startCountDown) {
			if (frozen && timeLeft > 0) {
				print(timeLeft);
				timeLeft -= Time.deltaTime;
			}
			else if (timeLeft <= 0) {
				UnFreeze();
			}
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.layer == LayerMask.NameToLayer("Freeze")) {
			Freeze();
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if (other.gameObject.layer == LayerMask.NameToLayer("Freeze")) {
			startCountDown = true;
		}
	}

	public void Freeze()
	{
		startCountUp = true;
		frozen = true;
	}

	public void UnFreeze()
	{
		timeLeft = frozenSec;
		timeFrozen = 0;
		frozen = false;
		startCountDown = false;
		startCountUp = false;
		stuckFrozen = false;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagnetEffect : MonoBehaviour {

	public struct MagnetContainer
	{
		public float initialDistance;
		public Vector3 position;
		public bool activated;
		public bool positive;
	}

	public bool activated;
	public bool positive;
	public bool renderIndicator;
	public Rigidbody2D attachedRigidbody;

	Collider2D magnetTrigger;
	float dragOrPullForce = 15000f;
	Dictionary<int, MagnetContainer> magnetsInMagnetField = new Dictionary<int, MagnetContainer>();

	void Awake()
	{
		magnetTrigger = this.GetComponent<Collider2D>();
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		// Add magnet to magnetfield
		if (other != magnetTrigger && 
			magnetsInMagnetField.ContainsKey(other.GetInstanceID()) == false) 
		{
			MagnetEffect mOther = other.GetComponent<MagnetEffect>();
			Vector2 heading = other.transform.position - this.transform.position;
			float distance = heading.magnitude;

			MagnetContainer mCon = new MagnetContainer();
			mCon.initialDistance = heading.magnitude;
			mCon.activated = mOther.activated;
			mCon.positive = mOther.positive;
			mCon.position = mOther.transform.position;

			magnetsInMagnetField.Add(other.GetInstanceID(), mCon);
		}
	}
	// Remove magnet from magnetfield
	void OnTriggerExist2D(Collider2D other)
	{
		if (magnetsInMagnetField.ContainsKey(other.GetInstanceID())) {
			magnetsInMagnetField.Remove(other.GetInstanceID());
		}
	}

	void FixedUpdate()
	{
		// Magnet drag or pull
		if (activated && attachedRigidbody != null) {
			foreach (MagnetContainer mCon in magnetsInMagnetField.Values) {
				if (mCon.activated == false)
					continue;

				Vector2 heading = mCon.position - this.transform.position;
				float currentDistance = heading.magnitude;
				Vector2 direction = heading / currentDistance;
				int pullOrDrag = (positive == mCon.positive) ? -1 : 1; // 1 = drag, -1 = pull
				float forceModifier = 0;

				if (currentDistance < mCon.initialDistance)
					forceModifier = (mCon.initialDistance - currentDistance) / mCon.initialDistance;

				Vector2 finalForce = direction * (dragOrPullForce * forceModifier) * pullOrDrag;
				attachedRigidbody.AddForce(finalForce);
			}
		}
	}

	void LateUpdate()
	{
		if (renderIndicator && activated) {
			// "debug"
			if (positive)
				gameObject.GetComponentInParent<SpriteRenderer>().color = Color.red;
			else
				gameObject.GetComponentInParent<SpriteRenderer>().color = Color.blue;
		}
	}
}
